Small, soft, ruby script designed to measure round trip message time in MQTT with fine granularity using inline POSIX nanosleeps.

The script requires a few Ruby dependencies. The client machine must have a recent version of ruby installed. Run `bundle install` to get all depending gems.

The nanosleeping function will *only* work on:

```SUSv2 to 4, Linux 2.5.63+, FreeBSD 3.0+, NetBSD 2.0+, OpenBSD 2.1+, macOS 10.12+```

(presumably, I'm suspicious about macOS  and the BSDs)
