require 'rubygems'
require 'mqtt'
require 'inline'

class Sleeper
  inline do |builder|
    builder.include '<time.h>'
    builder.c '
      void sleepy(long ns) {
        struct timespec ts = (struct timespec) {0,ns};
        if (clock_nanosleep(CLOCK_MONOTONIC, 0, &ts, NULL)) {
          /* Error occured, check errno */
        }
      }
      '
  end
end

class Connection
  def initialize
    begin 
      @client = MQTT::Client.connect(
        :host => '127.0.0.1',
        :port => 1833,
        :username => 'mqtt-test',
        :password => 'mqtt-test'
      )
    rescue => err
      puts "Fatal error when connecting: "
      puts "\n"
      puts err
      puts err.backtrace
    end
  end

  def client
    @client
  end

  def time
    "%.6f" % Time.now.to_f
  end
end

# sleeper = Sleeper.new
# for i in (0..).step(10000)
#   puts "sleeping for #{i} ns 100 times"
#   start = Time.now
#   for _ in (0..i)
#     sleeper.sleepy(i)
#   end
#   puts "\tit took #{(Time.now - start).to_f}s"
# end

begin
  puts "Press ctrl-C if you would like to exit the message loop"

  c = Connection.new
  sleeper = Sleeper.new
  i = 10000000
  begin
    puts "sleeping for #{i} ns 1000 times - "
    start = Time.now
    
    for _ in (0..1000)
      sleeper.sleepy(i)
    end

    zz = (Time.now - start).to_f
    puts "\t it took #{zz}s"
  
    puts "sending messages at #{i} ns rate 1000 times - "
    start = Time.now

    for _ in (0..1000)
      c.client.publish('test', 'message')
      c.client.get('test') do |topic,message|
        break
      end
      sleeper.sleepy(i)
    end

    result = ((Time.now - start).to_f - zz) / 1000
    puts "\t it took #{result}s"
    puts "------------------------------"
    puts "\n"
    File.write("log.txt", "#{result}\n", mode: "a")
    i = i / 2
  end until i < 1
  exit
rescue Interrupt => err
  puts "\n"
  puts "Got an interrupt; exiting."
  # puts "Average of all times: #{samples.inject{ |sum, el| sum + el }.to_f / samples.size}"
end
